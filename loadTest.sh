#!/bin/bash

# Define the endpoint URL
URL="http://localhost:3000/"

# Define the number of concurrent requests
CONCURRENCY=1000

# Define the total number of requests to send
TOTAL_REQUESTS=100

# Function to send a single request
send_request() {
    curl -s -o /dev/null -w "%{http_code}\n" "$URL"
}

# Loop to send concurrent requests
for ((i=1; i<=$CONCURRENCY; i++)); do
    send_request &
done

# Wait for all requests to finish
wait

echo "Load test complete."

